# This program performs an SQL query similar to the SQLRecoverPassword.py script. However,
# it has been altered so that it doesn't assemble the query using Python string operators.
# Rather it makes use of a tuple in a way described in https://docs.python.org/3/library/sqlite3.html
# For some reason it appears to stop the injection from working, but doesn't quite work
# as I thought (based on the description).
# Author: Warren Sutton
# Date: 9 Aug 2020
import sqlite3

connection = sqlite3.connect("students.db")
cursor = connection.cursor()

# This is a very trivial example of how to subvert a password recovery via an injection.
# If the code is run as is, then the user can recover his/her password. In this example
# when this code runs the user enters their id.The user should enter 1,2 or 3.
# This ID would be their studentID and the program would return their password.
# However, if instead of entering just a number such as "1","2" or "3" the user enters
# a string such as "3 OR id LIKE '%'" then the passwords for all users are returned.
my_id = input('What is your ID: ')

# Add the my_id input into a tuple
id_to_use = (my_id,)

temp = cursor.execute("SELECT * FROM student_details WHERE id is ?", id_to_use)

for raw in temp:
    print("your password is:", raw[2])
