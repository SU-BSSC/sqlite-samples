# This Password recovery tool should be run after the SQLite database is created.
# The database is called "students.db" and is created when the SQLCreate.py program is run
# This program can be run repeatedly is it only reads from the SQLite database ("students.db").
# The program can be used to demonstrate a very simplistic SQL injection.
# Author: Warren Sutton
# Date: 9 Aug 2020
import sqlite3

connection = sqlite3.connect("students.db")
cursor = connection.cursor()

# This is a very trivial example of how to subvert a password recovery via an injection.
# If the code is run as is, then the user can recover his/her password. In this example
# when this code runs the user enters their id.The user should enter 1,2 or 3.
# This ID would be their studentID and the program would return their password.
# However, if instead of entering just a number such as "1","2" or "3" the user enters
# a string such as "3 OR id LIKE '%'" then the passwords for all users are returned.
my_id = input('What is your ID: ')

select = f"SELECT * FROM student_details WHERE id is {my_id}"
temp = cursor.execute(select)

for raw in temp:
    print("your password is:", raw[2])
