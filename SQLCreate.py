# This program creates an SQLite database which is used to complete an SQL injection
# exercise. This code should be run only once to create the database and table required.
# Author: Warren Sutton
# Date: 9 Aug 2020
import sqlite3

connection = sqlite3.connect("students.db")
cursor = connection.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS student_details(id INTEGER, name TEXT, password TEXT)")

cursor.execute("INSERT INTO student_details(id, name, password) VALUES(1, 'student 1','mybigsecret')")
cursor.execute("INSERT INTO student_details(id, name, password) VALUES(2, 'student 2','donttellanyone')")
cursor.execute("INSERT INTO student_details(id, name, password) VALUES(3, 'student 3','theyllneverguessthis!#')")
connection.commit()

connection.close()